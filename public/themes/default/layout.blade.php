<!doctype html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$meta->title}}</title>
    <meta name="keywords" content="{{$meta->keywords}}">
    <meta name="description" content="{{$meta->description}}">
    <link href="https://lib.baomitu.com/tailwindcss/2.2.4/tailwind.min.css" rel="stylesheet">
</head>
<body class="text-sm text-gray-600">
<div class="mx-auto max-w-7xl  items-center gap-4 py-6 flex px-4" x-data="{menu: false}">
    {{--LOGO--}}
    <a href="/" class="flex-none flex  items-center gap-2">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 202.97 197.7" class="h-8 w-8 fill-current text-blue-600">
            <path
                d="M170,94.52l-35.9-20.73-24.34,14,11.62,6.71a5,5,0,0,1,0,8.66L32.5,154.52a5,5,0,0,1-7.5-4.33V99.61a6.44,6.44,0,0,1,0-1.52V47.51a5,5,0,0,1,7.5-4.33l35,20.23,24.32-14L7.5.68A5,5,0,0,0,0,5V192.69A5,5,0,0,0,7.5,197L170,103.18A5,5,0,0,0,170,94.52Z"></path>
            <path
                d="M32.93,103.18l35.9,20.73,24.34-14-11.62-6.71a5,5,0,0,1,0-8.66l88.92-51.34a5,5,0,0,1,7.5,4.33V98.09a6.44,6.44,0,0,1,0,1.52v50.58a5,5,0,0,1-7.5,4.33l-35-20.23-24.32,14L195.47,197a5,5,0,0,0,7.5-4.33V5a5,5,0,0,0-7.5-4.33L32.93,94.52A5,5,0,0,0,32.93,103.18Z"></path>
        </svg>
        <div class="text-2xl  text-gray-900">{{config('app.name')}}</div>
    </a>
    {{--移动端菜单开关--}}
    <div class="flex-grow justify-end flex lg:hidden">
        <div @click="menu = true" class="cursor-pointer text-gray-500">
            <svg class="h-6 w-6 " xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                 stroke="currentColor"
                 aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"/>
            </svg>
        </div>
    </div>
    {{--移动端菜单--}}
    <div class="absolute top-0 inset-x-0 p-2 transition transform origin-top-right lg:hidden " x-show="menu" x-cloak>
        <div class="rounded-lg shadow-md bg-white ring-1 ring-black ring-opacity-5 overflow-hidden">

            {{--菜单头部--}}
            <div class="px-5 pt-4 flex items-center justify-between">
                <a href="/" class="flex-none flex  items-center gap-2">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 202.97 197.7"
                         class="h-8 w-8 fill-current text-blue-600">
                        <path
                            d="M170,94.52l-35.9-20.73-24.34,14,11.62,6.71a5,5,0,0,1,0,8.66L32.5,154.52a5,5,0,0,1-7.5-4.33V99.61a6.44,6.44,0,0,1,0-1.52V47.51a5,5,0,0,1,7.5-4.33l35,20.23,24.32-14L7.5.68A5,5,0,0,0,0,5V192.69A5,5,0,0,0,7.5,197L170,103.18A5,5,0,0,0,170,94.52Z"></path>
                        <path
                            d="M32.93,103.18l35.9,20.73,24.34-14-11.62-6.71a5,5,0,0,1,0-8.66l88.92-51.34a5,5,0,0,1,7.5,4.33V98.09a6.44,6.44,0,0,1,0,1.52v50.58a5,5,0,0,1-7.5,4.33l-35-20.23-24.32,14L195.47,197a5,5,0,0,0,7.5-4.33V5a5,5,0,0,0-7.5-4.33L32.93,94.52A5,5,0,0,0,32.93,103.18Z"></path>
                    </svg>
                    <div class="text-2xl  text-gray-900">{{config('app.name')}}</div>
                </a>
                <div class="-mr-2">
                    <button type="button"
                            @click="menu = false"
                            class="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-500">
                        <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                             stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                  d="M6 18L18 6M6 6l12 12"/>
                        </svg>
                    </button>
                </div>
            </div>
            {{--菜单列表--}}
            <div class="px-2 pt-2 pb-3 space-y-1">
                <ul x-data="{open: 0}">
                    <li>
                        <a href="{{route('web.index')}}"
                           class="block px-3 py-2 hover:bg-blue-600 hover:text-white text-base">首页</a>
                    </li>
                    @articleclass()
                    <li @click="open = {{$item->class_id}}">
                        <a href="{{$item->children->count() ? 'javascript:;' : route('web.article.list', ['id' => $item->class_id])}}"
                           class="block px-3 py-2 flex items-center hover:bg-blue-600 hover:text-white text-base">
                            <div class="flex-grow">
                                {{$item->name}}
                            </div>
                            <div class="flex-none">
                                @if($item->children->count())
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none"
                                         viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M19 9l-7 7-7-7"/>
                                    </svg>
                                @endif
                            </div>
                        </a>
                        @if($item->children->count())
                            <div x-cloak class="p-2" x-show="open === {{$item->class_id}}">
                                <ul>
                                    @foreach($item->children as $vo)
                                        <li><a href="{{route('web.article.list', ['id' => $vo->class_id])}}"
                                               class="block px-4 py-2 hover:bg-blue-600 hover:text-white text-base">{{$vo->name}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </li>
                    @endarticleclass
                </ul>
            </div>
        </div>
    </div>

    {{--pc端菜单--}}
    <div class="flex-grow ml-10 w-1 hidden lg:block" x-data="{open: 0}">
        <ul class="flex flex-nowrap gap-10 items-center">
            <li class="whitespace-nowrap">
                <a class="block flex gap-3 items-center" href="{{route('web.index')}}">首页</a>
            </li>
            @articleclass(['limit' => 4])
            <li class="whitespace-nowrap relative" @mouseleave="open = 0">
                <a class="block flex gap-3 items-center" @mouseover="open = {{$item->class_id}}"
                   href="{{route('web.article.list', ['id' => $item->class_id])}}">{{$item->name}}
                    @if($item->children->count())
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24"
                             stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"/>
                        </svg>
                    @endif
                </a>
                @if($item->children->count())
                    <div x-cloak class="z-10 absolute w-52 bg-white shadow p-2 " x-show="open === {{$item->class_id}}">
                        <ul>
                            @foreach($item->children as $vo)
                                <li><a href="{{route('web.article.list', ['id' => $vo->class_id])}}"
                                       class="block px-4 py-2 hover:bg-blue-600 hover:text-white">{{$vo->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </li>
            @endarticleclass
        </ul>
    </div>

    {{--搜索组件--}}
    <div class="flex-none hidden lg:block">
        <form action="{{route('web.article.search')}}" class="flex gap-4 border h-10  items-center px-2">
            <div>
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 text-gray-400" fill="none" viewBox="0 0 24 24"
                     stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                          d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"/>
                </svg>
            </div>
            <input class="block h-8 outline-none w-24" type="text" name="keyword" value="{{$keyword}}">
        </form>
    </div>


    {{--快捷工具--}}
    <div class="flex-none flex gap-4 hidden lg:flex">
        <a href="https://qm.qq.com/cgi-bin/qm/qr?k=DY0Lqzv53YDNA85lWav1ky3Od-gvxU04&jump_from=webapi"
           target="_blank">
            <svg class="w-6 h-6 fill-current  text-gray-600 hover:text-blue-600" viewBox="0 0 1024 1024" version="1.1"
                 xmlns="http://www.w3.org/2000/svg" p-id="2421" width="200" height="200">
                <path
                    d="M512.5681755 3.6315227c-280.7266725 0-508.30211299 227.57427541-508.30211299 508.302113s227.57427541 508.30211299 508.30211299 508.30211299 508.30094791-227.57427541 508.30094791-508.30211299S793.29484914 3.6315227 512.5681755 3.6315227zM444.32651491 712.69303069l-0.07451534 0.06287246c-8.02315719 6.83208363-13.30090667 13.75614635-16.70996309 20.28318378-5.73881003-0.01047893-11.3623552-0.31785301-16.97076451-0.79404942-49.61876195 27.72186567-135.01747541 18.49715598-156.10285967-0.3620955-16.31177387-14.56067357-15.46300302-33.57361266 3.77115193-49.95291478 5.09728427-4.29857678 14.07283427-8.03247104 24.43855303-11.18538296-16.90090723-18.43544747-29.73958827-40.17982237-37.41578581-63.94890582-4.47904313 7.10802091-8.51216157 13.63505949-11.18421788 18.3399754-10.38201856 18.31086877-23.00414179 20.08176071-30.20064882 0.26778738-8.00336441-21.96442567-5.26959957-55.56947399 9.46804736-93.55343645 9.11410176-23.46869533 24.36287374-43.37697678 35.48654933-56.24360163-4.10996167-7.20000114-6.45834411-14.73066098-6.45834411-22.52910934 0-16.28383005 9.57749134-31.47089579 25.99987314-44.52380672 8.35731001-84.35783339 76.52911331-150.14050475 159.45253547-150.14050474 44.08719701 0 84.01436672 18.61358478 112.99250631 48.72807651-12.68965149 7.16972942-24.60970439 15.95200626-35.47374137 26.25834667-30.86429753 29.27736377-50.92044459 68.49082027-57.02833607 111.17038478-22.21824227 20.36119211-27.11992434 40.86326386-27.11992434 54.98616263 0 6.43040142 1.03855104 12.84101006 3.09818937 19.17244644-13.73751751 17.44346795-24.05084274 34.92768768-30.72458183 52.11151815-16.30129493 42.01475072-19.71733618 80.41553123-9.62522681 108.11178098 8.10815033 22.32186425 22.21125632 26.76481479 29.89676885 27.42497052 11.33091954 0.96985771 20.26455495-4.55937934 26.79857949-11.2703761 2.68370034 4.74683051 5.57697365 9.38771115 8.67167004 13.91216184C447.48873955 710.20027563 445.83311246 711.42278485 444.32651491 712.69303069zM821.80610617 656.69509575c-6.31862955 17.34217501-17.36080384 15.79016989-26.45394774-0.23518777-2.34023253-4.12160455-5.87037582-9.83945785-9.79637816-16.06494321-6.68538197 20.71164473-17.82186439 39.63493262-32.5152677 55.72781852 9.48434717 2.84553671 17.69728455 6.18357077 22.31371434 10.0862885 16.8322139 14.34760761 17.5901696 31.00284814 3.31591226 43.75653717-18.48434802 16.51901781-93.41139171 24.61319737-136.84774799 0.27477333-5.31850013 0.46571747-10.7045285 0.74049195-16.15808625 0.74049195-5.11358407 0-10.12005205-0.27477333-15.11371435-0.69857735-43.46429895 24.28253753-118.26909184 16.20232989-136.73946795-0.31668793-14.28822926-12.75368789-13.54424434-29.40892843 3.30310542-43.75653717 4.46507122-3.76532992 12.32755598-7.03699968 21.4067291-9.79754326-14.80401123-16.14877241-26.04993763-35.19547506-32.77490517-56.01656376-3.92367445 6.22548537-7.45614563 11.94333867-9.79637817 16.06494321-9.09430898 16.03932843-20.15045405 17.59133355-26.45394773 0.23518777-7.01022094-19.23997582-4.61526585-48.67684807 8.2932736-81.94890752 7.98357163-20.55679317 21.34036366-37.99676928 31.08434944-49.26714538-3.60000057-6.30698667-5.65730987-12.90388253-5.65730987-19.73480107 0-14.26377842 8.38990962-27.56701411 22.77477604-39.00039168 7.31992291-73.89431353 67.03661625-131.51644103 139.67349191-131.51644103 72.61126087 0 132.34192611 57.63609827 139.66184903 131.51644103 14.37089337 11.43337757 22.77477603 24.73661213 22.77477604 39.00039168 0 6.83091854-2.04333739 13.4278144-5.65730987 19.73480107 9.74398578 11.28434802 23.11474973 28.70918713 31.08434944 49.26714538l-0.01397191 0C826.42137202 608.01824882 828.81632711 637.45511993 821.80610617 656.69509575z"></path>
            </svg>
        </a>

        <a href="https://support.qq.com/products/331847" target="_blank">
            <svg class="h-6 w-6 text-gray-600 fill-current hover:text-blue-600" viewBox="0 0 1024 1024" version="1.1"
                 xmlns="http://www.w3.org/2000/svg" p-id="4728" width="200" height="200">
                <path
                    d="M305.984 390.336c-21.696 0-38.56 16.864-38.56 38.56s16.864 38.56 38.56 38.56 38.56-16.864 38.56-38.56-16.864-38.56-38.56-38.56z m284.32 77.088c21.696 0 38.56-16.864 38.56-38.56s-18.08-38.56-38.56-38.56c-21.696 0-38.56 16.864-38.56 38.56s16.864 38.56 38.56 38.56z m-142.144 0c21.696 0 38.56-16.864 38.56-38.56s-18.08-38.56-38.56-38.56c-21.696 0-38.56 16.864-38.56 38.56s16.864 38.56 38.56 38.56zM512 10.848C234.912 10.848 10.848 234.912 10.848 512S234.912 1013.152 512 1013.152 1013.152 789.088 1013.152 512 789.088 10.848 512 10.848zM202.4 699.936c27.712-19.264 37.344-33.728 45.792-51.808-39.744-3.616-71.072-36.128-71.072-77.088V286.72a76.864 76.864 0 0 1 77.088-77.088h386.72a76.864 76.864 0 0 1 77.088 77.088v284.32c1.216 42.176-33.728 77.088-77.088 77.088h-257.792c-59.04 56.608-102.4 77.088-155.392 77.088-19.264 0-39.744-13.248-25.312-25.312z m644.512-53.024c-1.216 44.576-32.512 81.92-74.688 92.768 7.232 19.264 19.264 40.96 50.592 62.656-1.216 9.632-85.536 39.744-180.704-51.808h-233.728c-24.096 0-51.808-7.232-65.056-18.08a160.736 160.736 0 0 0 51.808-33.728H614.4c110.848 0 155.392-49.408 155.392-128.896v-244.544c50.592 4.832 77.088 40.96 77.088 90.368v231.296z"
                    p-id="4729"></path>
            </svg>
        </a>
        <a href="https://github.com/duxphp/CMSRavel/issues" target="_blank">
            <svg class="h-6 w-6 text-gray-600 fill-current hover:text-blue-600" viewBox="0 0 1024 1024" version="1.1"
                 xmlns="http://www.w3.org/2000/svg" p-id="4598" width="200" height="200">
                <path
                    d="M0 520.886c0-69.368 13.51-135.697 40.498-199.02 26.987-63.323 63.322-117.826 109.006-163.51 45.65-45.65 100.154-81.985 163.51-109.006A502.289 502.289 0 0 1 512 8.92c69.335 0 135.663 13.477 198.986 40.497 63.356 26.988 117.86 63.323 163.51 109.007 45.684 45.65 82.02 100.154 109.006 163.51A502.289 502.289 0 0 1 1024 520.852c0 111.318-32.504 211.472-97.511 300.494-64.975 88.989-148.48 150.825-250.484 185.476-5.351 0-9.348-0.99-11.99-2.973-2.676-1.982-4.196-3.997-4.526-6.012a59.458 59.458 0 0 1-0.495-8.984 7.663 7.663 0 0 1-0.991-3.006v-128.99c0-40.63-14.336-75.314-43.008-103.986 76.667-13.345 134.011-41.819 171.999-85.487 37.987-43.669 57.013-96.52 57.013-158.522 0-58.005-18.663-108.346-56.022-150.99 13.345-42.678 11-87.668-6.97-135.003-18.697-1.322-39.011 1.85-61.01 9.513-22 7.663-38.318 14.831-49.02 21.47-10.637 6.673-20.316 13.016-28.97 19.027-38.68-10.669-81.854-16.02-129.486-16.02-47.7 0-90.509 5.351-128.529 16.02-7.333-5.35-15.855-11.164-25.5-17.507-9.68-6.342-26.493-14.005-50.507-22.99-23.982-9.018-45.65-12.85-65.008-11.495-18.663 47.996-20.645 93.646-5.979 136.984-36.665 42.678-54.998 92.986-54.998 150.99 0 62.002 18.663 114.689 55.99 157.994 37.326 43.339 94.67 72.01 171.998 86.016a142.303 142.303 0 0 0-39.969 70.029c-56.683 13.972-96.355 3.963-119.015-30.06-42.017-61.308-79.674-83.307-113.003-65.965-4.69 4.657-3.997 9.48 1.982 14.501 6.012 4.988 14.996 11.66 27.02 19.985 11.99 8.357 20.976 17.507 26.987 27.515 0.661 1.322 2.51 6.177 5.517 14.502a831.917 831.917 0 0 0 8.985 23.981c2.973 7.663 8.654 16.186 17.011 25.5 8.324 9.349 18.003 17.178 29.003 23.52 11 6.309 26.161 11 45.485 14.006 19.324 2.972 41.323 3.138 65.998 0.495v100.484c0 0.991-0.165 2.643-0.495 5.021-0.33 2.312-0.991 3.964-1.982 4.955-0.991 1.024-2.345 2.015-4.03 3.039a12.52 12.52 0 0 1-6.474 1.486c-2.676 0-6.012-0.33-10.009-0.99-101.343-35.345-183.825-97.182-247.51-185.51C31.842 731.037 0 631.577 0 520.92z"
                    p-id="4599"></path>
            </svg>
        </a>
    </div>
</div>

{{--布局插槽--}}
@yield('content')

<div class="mt-16">
    <div class="mx-auto  max-w-7xl px-4">
        <div class=" flex flex-col lg:flex-row gap-8 py-6  border-t border-gray-200">
            {{--自定义菜单--}}
            <div class="flex-none">
                <div class="text-base font-bold">友情链接</div>
                <div class="grid grid-cols-2 lg:grid-cols-3 gap-x-10 gap-y-4 mt-6">
                    @menu(['id' => 1, 'limit' => 6])
                    <div>
                        <a href="{{$item->url}}" class="hover:underline" target="_blank">{{$item->name}}</a>
                    </div>
                    @endmenu
                </div>
            </div>
            <div class="flex-grow lg:justify-end flex">
                {{--反馈工具--}}
                <div class="lg:text-right">
                    <div class="text-base font-bold">建议和反馈</div>
                    <div class="text-sm mt-6">如果您对该产品有什么建议或者想法您可以采用以下方式跟我们进行沟通</div>
                    <div class="flex gap-2 lg:justify-end mt-4">
                        <a href="https://qm.qq.com/cgi-bin/qm/qr?k=DY0Lqzv53YDNA85lWav1ky3Od-gvxU04&jump_from=webapi"
                           target="_blank">
                            <svg class="w-6 h-6 fill-current  text-blue-600" viewBox="0 0 1024 1024" version="1.1"
                                 xmlns="http://www.w3.org/2000/svg" p-id="2421" width="200" height="200">
                                <path
                                    d="M512.5681755 3.6315227c-280.7266725 0-508.30211299 227.57427541-508.30211299 508.302113s227.57427541 508.30211299 508.30211299 508.30211299 508.30094791-227.57427541 508.30094791-508.30211299S793.29484914 3.6315227 512.5681755 3.6315227zM444.32651491 712.69303069l-0.07451534 0.06287246c-8.02315719 6.83208363-13.30090667 13.75614635-16.70996309 20.28318378-5.73881003-0.01047893-11.3623552-0.31785301-16.97076451-0.79404942-49.61876195 27.72186567-135.01747541 18.49715598-156.10285967-0.3620955-16.31177387-14.56067357-15.46300302-33.57361266 3.77115193-49.95291478 5.09728427-4.29857678 14.07283427-8.03247104 24.43855303-11.18538296-16.90090723-18.43544747-29.73958827-40.17982237-37.41578581-63.94890582-4.47904313 7.10802091-8.51216157 13.63505949-11.18421788 18.3399754-10.38201856 18.31086877-23.00414179 20.08176071-30.20064882 0.26778738-8.00336441-21.96442567-5.26959957-55.56947399 9.46804736-93.55343645 9.11410176-23.46869533 24.36287374-43.37697678 35.48654933-56.24360163-4.10996167-7.20000114-6.45834411-14.73066098-6.45834411-22.52910934 0-16.28383005 9.57749134-31.47089579 25.99987314-44.52380672 8.35731001-84.35783339 76.52911331-150.14050475 159.45253547-150.14050474 44.08719701 0 84.01436672 18.61358478 112.99250631 48.72807651-12.68965149 7.16972942-24.60970439 15.95200626-35.47374137 26.25834667-30.86429753 29.27736377-50.92044459 68.49082027-57.02833607 111.17038478-22.21824227 20.36119211-27.11992434 40.86326386-27.11992434 54.98616263 0 6.43040142 1.03855104 12.84101006 3.09818937 19.17244644-13.73751751 17.44346795-24.05084274 34.92768768-30.72458183 52.11151815-16.30129493 42.01475072-19.71733618 80.41553123-9.62522681 108.11178098 8.10815033 22.32186425 22.21125632 26.76481479 29.89676885 27.42497052 11.33091954 0.96985771 20.26455495-4.55937934 26.79857949-11.2703761 2.68370034 4.74683051 5.57697365 9.38771115 8.67167004 13.91216184C447.48873955 710.20027563 445.83311246 711.42278485 444.32651491 712.69303069zM821.80610617 656.69509575c-6.31862955 17.34217501-17.36080384 15.79016989-26.45394774-0.23518777-2.34023253-4.12160455-5.87037582-9.83945785-9.79637816-16.06494321-6.68538197 20.71164473-17.82186439 39.63493262-32.5152677 55.72781852 9.48434717 2.84553671 17.69728455 6.18357077 22.31371434 10.0862885 16.8322139 14.34760761 17.5901696 31.00284814 3.31591226 43.75653717-18.48434802 16.51901781-93.41139171 24.61319737-136.84774799 0.27477333-5.31850013 0.46571747-10.7045285 0.74049195-16.15808625 0.74049195-5.11358407 0-10.12005205-0.27477333-15.11371435-0.69857735-43.46429895 24.28253753-118.26909184 16.20232989-136.73946795-0.31668793-14.28822926-12.75368789-13.54424434-29.40892843 3.30310542-43.75653717 4.46507122-3.76532992 12.32755598-7.03699968 21.4067291-9.79754326-14.80401123-16.14877241-26.04993763-35.19547506-32.77490517-56.01656376-3.92367445 6.22548537-7.45614563 11.94333867-9.79637817 16.06494321-9.09430898 16.03932843-20.15045405 17.59133355-26.45394773 0.23518777-7.01022094-19.23997582-4.61526585-48.67684807 8.2932736-81.94890752 7.98357163-20.55679317 21.34036366-37.99676928 31.08434944-49.26714538-3.60000057-6.30698667-5.65730987-12.90388253-5.65730987-19.73480107 0-14.26377842 8.38990962-27.56701411 22.77477604-39.00039168 7.31992291-73.89431353 67.03661625-131.51644103 139.67349191-131.51644103 72.61126087 0 132.34192611 57.63609827 139.66184903 131.51644103 14.37089337 11.43337757 22.77477603 24.73661213 22.77477604 39.00039168 0 6.83091854-2.04333739 13.4278144-5.65730987 19.73480107 9.74398578 11.28434802 23.11474973 28.70918713 31.08434944 49.26714538l-0.01397191 0C826.42137202 608.01824882 828.81632711 637.45511993 821.80610617 656.69509575z"></path>
                            </svg>
                        </a>

                        <a href="https://support.qq.com/products/331847" target="_blank">
                            <svg class="h-6 w-6 text-purple-500 fill-current" viewBox="0 0 1024 1024" version="1.1"
                                 xmlns="http://www.w3.org/2000/svg" p-id="4728" width="200" height="200">
                                <path
                                    d="M305.984 390.336c-21.696 0-38.56 16.864-38.56 38.56s16.864 38.56 38.56 38.56 38.56-16.864 38.56-38.56-16.864-38.56-38.56-38.56z m284.32 77.088c21.696 0 38.56-16.864 38.56-38.56s-18.08-38.56-38.56-38.56c-21.696 0-38.56 16.864-38.56 38.56s16.864 38.56 38.56 38.56z m-142.144 0c21.696 0 38.56-16.864 38.56-38.56s-18.08-38.56-38.56-38.56c-21.696 0-38.56 16.864-38.56 38.56s16.864 38.56 38.56 38.56zM512 10.848C234.912 10.848 10.848 234.912 10.848 512S234.912 1013.152 512 1013.152 1013.152 789.088 1013.152 512 789.088 10.848 512 10.848zM202.4 699.936c27.712-19.264 37.344-33.728 45.792-51.808-39.744-3.616-71.072-36.128-71.072-77.088V286.72a76.864 76.864 0 0 1 77.088-77.088h386.72a76.864 76.864 0 0 1 77.088 77.088v284.32c1.216 42.176-33.728 77.088-77.088 77.088h-257.792c-59.04 56.608-102.4 77.088-155.392 77.088-19.264 0-39.744-13.248-25.312-25.312z m644.512-53.024c-1.216 44.576-32.512 81.92-74.688 92.768 7.232 19.264 19.264 40.96 50.592 62.656-1.216 9.632-85.536 39.744-180.704-51.808h-233.728c-24.096 0-51.808-7.232-65.056-18.08a160.736 160.736 0 0 0 51.808-33.728H614.4c110.848 0 155.392-49.408 155.392-128.896v-244.544c50.592 4.832 77.088 40.96 77.088 90.368v231.296z"
                                    p-id="4729"></path>
                            </svg>
                        </a>
                        <a href="https://github.com/duxphp/CMSRavel/issues" target="_blank">
                            <svg class="h-6 w-6 text-gray-500 fill-current" viewBox="0 0 1024 1024" version="1.1"
                                 xmlns="http://www.w3.org/2000/svg" p-id="4598" width="200" height="200">
                                <path
                                    d="M0 520.886c0-69.368 13.51-135.697 40.498-199.02 26.987-63.323 63.322-117.826 109.006-163.51 45.65-45.65 100.154-81.985 163.51-109.006A502.289 502.289 0 0 1 512 8.92c69.335 0 135.663 13.477 198.986 40.497 63.356 26.988 117.86 63.323 163.51 109.007 45.684 45.65 82.02 100.154 109.006 163.51A502.289 502.289 0 0 1 1024 520.852c0 111.318-32.504 211.472-97.511 300.494-64.975 88.989-148.48 150.825-250.484 185.476-5.351 0-9.348-0.99-11.99-2.973-2.676-1.982-4.196-3.997-4.526-6.012a59.458 59.458 0 0 1-0.495-8.984 7.663 7.663 0 0 1-0.991-3.006v-128.99c0-40.63-14.336-75.314-43.008-103.986 76.667-13.345 134.011-41.819 171.999-85.487 37.987-43.669 57.013-96.52 57.013-158.522 0-58.005-18.663-108.346-56.022-150.99 13.345-42.678 11-87.668-6.97-135.003-18.697-1.322-39.011 1.85-61.01 9.513-22 7.663-38.318 14.831-49.02 21.47-10.637 6.673-20.316 13.016-28.97 19.027-38.68-10.669-81.854-16.02-129.486-16.02-47.7 0-90.509 5.351-128.529 16.02-7.333-5.35-15.855-11.164-25.5-17.507-9.68-6.342-26.493-14.005-50.507-22.99-23.982-9.018-45.65-12.85-65.008-11.495-18.663 47.996-20.645 93.646-5.979 136.984-36.665 42.678-54.998 92.986-54.998 150.99 0 62.002 18.663 114.689 55.99 157.994 37.326 43.339 94.67 72.01 171.998 86.016a142.303 142.303 0 0 0-39.969 70.029c-56.683 13.972-96.355 3.963-119.015-30.06-42.017-61.308-79.674-83.307-113.003-65.965-4.69 4.657-3.997 9.48 1.982 14.501 6.012 4.988 14.996 11.66 27.02 19.985 11.99 8.357 20.976 17.507 26.987 27.515 0.661 1.322 2.51 6.177 5.517 14.502a831.917 831.917 0 0 0 8.985 23.981c2.973 7.663 8.654 16.186 17.011 25.5 8.324 9.349 18.003 17.178 29.003 23.52 11 6.309 26.161 11 45.485 14.006 19.324 2.972 41.323 3.138 65.998 0.495v100.484c0 0.991-0.165 2.643-0.495 5.021-0.33 2.312-0.991 3.964-1.982 4.955-0.991 1.024-2.345 2.015-4.03 3.039a12.52 12.52 0 0 1-6.474 1.486c-2.676 0-6.012-0.33-10.009-0.99-101.343-35.345-183.825-97.182-247.51-185.51C31.842 731.037 0 631.577 0 520.92z"
                                    p-id="4599"></path>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class=" border-t border-gray-200 py-4 flex items-center">
            <div class="flex-grow text-center lg:text-left">© Copyright 2021, All Rights Reserved</div>
            <div class="flex-none hidden lg:flex space-x-6">
                {{--自定义菜单--}}
                @menu(['id' => 2])
                <a href="{{$item->url}}" class="hover:underline">{{$item->name}}</a>
                @endmenu
            </div>
        </div>
    </div>
</div>

</body>
<script src="https://lib.baomitu.com/alpinejs/3.2.1/cdn.min.js" defer></script>
</html>
